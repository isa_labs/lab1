#include<stdio.h>
#include<stdlib.h>

#define N 2 /// order of the filter 
#define NB 9  /// number of bits

const int b0 = 52; /// coefficient b0
const int b[N]={105, 52}; /// b array
const int a[N]={-95, 50}; /// a array

/// Perform fixed point filtering assuming direct form II
///\param x is the new input sample
///\return the new output sample
int myfilter(int x, char* filename)
{
  static int sw[N]; /// w shift register
  static int first_run = 0; /// for cleaning the shift register
  int i; /// index
  int w; /// intermediate value (w)
  int y; /// output sample
  int fb, ff; /// feed-back and feed-forward results
  FILE *fp_dbg;

  fp_dbg = fopen(filename, "a");

  fprintf(fp_dbg, "x = %d\n", x);
  /// clean the buffer
  if (first_run == 0)
  {
    first_run = 1;
    for (i=0; i<N; i++)
      sw[i] = 0;
  }

  /// compute feed-back and feed-forward
  fb = 0;
  ff = 0;
  for (i=0; i<N; i++)
  {
    fb += -(sw[i]*a[i]) >> (NB-1);  // the behaviour of the accumulate is slightly modified to produce the expected result 
    fprintf(fp_dbg, "sw[%d]*a[%d] = %d", i, i, sw[i] * a[i]);
    fprintf(fp_dbg, "fb %d = %d\n", i, fb);
    ff += (sw[i]*b[i]) >> (NB-1);
    fprintf(fp_dbg, "ff %d = %d\n", i, ff);
  }

  fprintf(fp_dbg, "ff = %d\n", ff);
  fprintf(fp_dbg, "fb = %d\n", fb);

  /// compute intermediate value (w) and output sample
  w = x + fb;
  fprintf(fp_dbg, "w = %d\n", w);
  y = (w*b0) >> (NB-1);
  fprintf(fp_dbg, "w * b0 = %d\n", y);
  y += ff;

  /// update the shift register
  for (i=N-1; i>0; i--)
    sw[i] = sw[i-1];
  sw[0] = w;

  fclose(fp_dbg);

  return y;
}

int main (int argc, char **argv)
{
  FILE *fp_in;
  FILE *fp_out;
  
  int x;
  int y;

  /// check the command line
  if (argc != 4)
  {
    printf("Use: %s <input_file> <output_file>\n", argv[0]);
    exit(1);
  }

  /// open files
  fp_in = fopen(argv[1], "r");
  if (fp_in == NULL)
  {
    printf("Error: cannot open %s\n", argv[1]);
    exit(2);
  }
  fp_out = fopen(argv[2], "w");

  /// get samples and apply filter
  fscanf(fp_in, "%d", &x);
  do{
    y = myfilter(x, argv[3]);
    //printf("%d", y);
    fprintf(fp_out,"%d\n", y);
    fscanf(fp_in, "%d", &x);
  } while (!feof(fp_in));

  fclose(fp_in);
  fclose(fp_out);

  return 0;

}
