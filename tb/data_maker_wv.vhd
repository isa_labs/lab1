library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_textio.all;

library std;
use std.textio.all;

entity data_maker_wv is  
  port (
    CLK     : in  std_logic;
    RST_n   : in  std_logic;
    VOUT    : out std_logic;
    DOUT    : out std_logic_vector(8 downto 0);
    A1: out std_logic_vector(8 downto 0);
    A2: out std_logic_vector(8 downto 0);
    B0: out std_logic_vector(8 downto 0);
    B1: out std_logic_vector(8 downto 0);
    B2: out std_logic_vector(8 downto 0);
    END_SIM : out std_logic);
end data_maker_wv;

architecture beh of data_maker_wv is

  constant tco : time := 1 ns;

  signal sEndSim : std_logic;
  signal vin_down : std_logic := '1';
  signal END_SIM_i : std_logic_vector(0 to 10);  

begin  -- beh

  B0 <= std_logic_vector(to_signed(52, 9));
  B1 <= std_logic_vector(to_signed(105, 9));
  B2 <= std_logic_vector(to_signed(52, 9));
  A1 <= std_logic_vector(to_signed(-95, 9));  
  A2 <= std_logic_vector(to_signed(50, 9));  

  process --(RST_n)
  begin
    wait for 100 ns;
    vin_down <= not vin_down;
    wait for 20 ns;
    vin_down <= not vin_down;
    wait;
  end process;

  process (CLK, RST_n)
    file fp_in : text open READ_MODE is "../matlab/samples.txt";
    variable line_in : line;
    variable x : integer;
  begin  -- process
    if RST_n = '0' then                 -- asynchronous reset (active low)
      DOUT <= (others => '0') after tco;      
      VOUT <= '0' after tco;
      sEndSim <= '0' after tco;
    elsif CLK'event and CLK = '1' then  -- rising clock edge
      if vin_down = '1' then
        if not endfile(fp_in) then
          readline(fp_in, line_in);
          read(line_in, x);
          DOUT <= std_logic_vector(to_signed(x, 9)) after tco;
          VOUT <= '1' after tco;
          sEndSim <= '0' after tco;
        else
          VOUT <= '0' after tco;        
          sEndSim <= '1' after tco;
        end if;
      else
        VOUT <= '0' after tco;
      end if;
    end if;
  end process;

  process (CLK, RST_n)
  begin  -- process
    if RST_n = '0' then                 -- asynchronous reset (active low)
      END_SIM_i <= (others => '0') after tco;
    elsif CLK'event and CLK = '1' then  -- rising clock edge
      END_SIM_i(0) <= sEndSim after tco;
      END_SIM_i(1 to 10) <= END_SIM_i(0 to 9) after tco;
    end if;
  end process;

  END_SIM <= END_SIM_i(10);  

end beh;
