library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity iir_filter is
  port (CLK: in std_logic;
        RST_n: in std_logic;
        DIN: in std_logic_vector(8 downto 0);
        VIN: in std_logic;
        A1: in std_logic_vector(8 downto 0);
        A2: in std_logic_vector(8 downto 0);
        B0: in std_logic_vector(8 downto 0);
        B1: in std_logic_vector(8 downto 0);
        B2: in std_logic_vector(8 downto 0);
        DOUT: out std_logic_vector(8 downto 0);
        VOUT: out std_logic);
end iir_filter;

architecture strct of iir_filter is

  component general_register  is   --asynchronous N bit register
    generic(N : integer := 32);
    port (D :	in std_logic_vector(N-1 downto 0);
          clk :	in std_logic;
          rst :	in std_logic;
          en : in std_logic;
          Q :	out	std_logic_vector(N-1 downto 0));
  end component;

  component flipflopD is   
	port (D :	in std_logic;
		  clk :	in std_logic;
		  rst :	in std_logic; -- reset active low
		  en : in std_logic;
		  Q :	out	std_logic);
  end component;

  signal input: std_logic_vector(8 downto 0);
  
  signal out_add_back0: signed(8 downto 0);
  signal out_add_back1: signed(8 downto 0);

  signal out_mul_back1: signed(8 downto 0);
  signal out_mul_back2: signed(8 downto 0);

  signal out_mul_back1_t: signed(17 downto 0);
  signal out_mul_back2_t: signed(17 downto 0);

  signal out_mul_fw0: signed(8 downto 0);
  signal out_mul_fw1: signed(8 downto 0);
  signal out_mul_fw2: signed(8 downto 0);

  signal out_mul_fw0_t: signed(17 downto 0);
  signal out_mul_fw1_t: signed(17 downto 0);
  signal out_mul_fw2_t: signed(17 downto 0);

  signal out_add_fw0: signed(8 downto 0);
  signal out_add_fw1: signed(8 downto 0);

  signal out_reg0: std_logic_vector(8 downto 0);
  signal out_reg1: std_logic_vector(8 downto 0);

  signal vin1, vin2: std_logic;
begin

  regin: general_register 
    generic map (9)
    port map (DIN, CLK, RST_n, VIN, input);

  regout: general_register
    generic map (9)
    port map (std_logic_vector(out_add_fw0), CLK, RST_n, '1', DOUT);

  reg0: general_register
    generic map (9)
    port map (std_logic_vector(out_add_back0), CLK, RST_n, vin, out_reg0); -- the internal register uses the VIN signal as an enable

  reg1: general_register
    generic map (9)
    port map (out_reg0, CLK, RST_n, vin, out_reg1); -- the internal register uses the VIN signal as an enable

-- the filter produces results in two clock cycles. The signal VIN must be delayed accordingly in order to
-- synchronize the output register. Two flip flops are added to this avail.

  reg_vin1: flipflopD
    port map (VIN, CLK, RST_n, '1', vin1); 
  
  reg_vin2: flipflopD
    port map (vin1, CLK, RST_n, '1', vin2);

  out_add_back0 <= signed(input) + out_add_back1;
  out_add_back1 <= out_mul_back1 + out_mul_back2;
  
  out_mul_back1_t <= signed(out_reg0) * (-signed(A1));
  out_mul_back2_t <= signed(out_reg1) * (-signed(A2));

  out_mul_back1 <= out_mul_back1_t(16 downto 8);
  out_mul_back2 <= out_mul_back2_t(16 downto 8);

  out_add_fw0 <= out_add_fw1 + out_mul_fw0;
  out_add_fw1 <= out_mul_fw1 + out_mul_fw2;

  out_mul_fw0_t <= out_add_back0 * signed(B0);
  out_mul_fw1_t <= signed(out_reg0) * signed(B1);
  out_mul_fw2_t <= signed(out_reg1) * signed(B2);

  out_mul_fw0 <= out_mul_fw0_t(16 downto 8);
  out_mul_fw1 <= out_mul_fw1_t(16 downto 8);
  out_mul_fw2 <= out_mul_fw2_t(16 downto 8);


  
  VOUT <= vin2 and RST_n;

end strct;
