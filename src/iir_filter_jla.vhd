library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity iir_filter_jla is
  port (CLK: in std_logic;
        RST_n: in std_logic;
        DIN: in std_logic_vector(8 downto 0);
        VIN: in std_logic;
        A1: in std_logic_vector(16 downto 0);
        A1_A2: in std_logic_vector(16 downto 0);
        A1_2_A2: in std_logic_vector(16 downto 0);
        B0: in std_logic_vector(16 downto 0);
        B1: in std_logic_vector(16 downto 0);
        B2: in std_logic_vector(16 downto 0);
        DOUT: out std_logic_vector(8 downto 0);
        VOUT: out std_logic);
end iir_filter_jla;

architecture strct of iir_filter_jla is

  component general_register  is   --asynchronous N bit register
    generic(N : integer := 32);
    port (D :	in std_logic_vector(N-1 downto 0);
          clk :	in std_logic;
          rst :	in std_logic;
          en : in std_logic;
          Q :	out	std_logic_vector(N-1 downto 0));
  end component;

  component flipflopD is   
	port (D :	in std_logic;
		  clk :	in std_logic;
		  rst :	in std_logic; -- reset active low
		  en : in std_logic;
		  Q :	out	std_logic);
  end component;

  signal DIN_EXT, DOUT_EXT, input: std_logic_vector(8 downto 0);
  
  signal out_add_1, out_add_2, out_add_3, out_add_4, out_add_5: signed(8 downto 0);

  signal out_mul_1, out_mul_2, out_mul_3, out_mul_4, out_mul_5, out_mul_6: signed(8 downto 0);

  signal out_mul_1_t, out_mul_2_t, out_mul_3_t, out_mul_4_t, out_mul_5_t, out_mul_6_t: signed(25 downto 0);
  
  signal out_reg_1, out_reg_2, out_reg_3, out_reg_4, out_reg_5, out_reg_6, out_reg_7, out_reg_8, out_reg_9, out_reg_10, out_reg_11, out_reg_12, out_reg_13 : std_logic_vector(8 downto 0);

  signal vin1, vin2, vin3, vin4, vin5, vin6: std_logic;
begin

  DIN_EXT <= DIN; --& "00000000";
  DOUT <= DOUT_EXT;--(16 downto 8);

  regin: general_register 
    generic map (9)
    port map (DIN_EXT, CLK, RST_n, VIN, input);

  --reg_1: general_register 
    --generic map (9)
    --port map (input, CLK, RST_n, vin1, out_reg_1);

  --reg_2: general_register 
    --generic map (9)
    --port map (input, CLK, RST_n, vin1, out_reg_2);

  reg_1: general_register 
    generic map (9)
    port map (std_logic_vector(out_mul_1), CLK, RST_n, VIN, out_reg_1);

  reg_2: general_register 
    generic map (9)
    port map (std_logic_vector(out_mul_2), CLK, RST_n, vin1, out_reg_2);

  reg_3: general_register 
    generic map (9)
    port map (std_logic_vector(out_mul_3), CLK, RST_n, vin1, out_reg_3);

  reg_4: general_register 
    generic map (9)
    port map (std_logic_vector(out_add_3), CLK, RST_n, vin1, out_reg_4);

  reg_5: general_register 
    generic map (9)
    port map (out_reg_4, CLK, RST_n, vin1, out_reg_5);

  reg_6: general_register 
    generic map (9)
    port map (out_reg_4, CLK, RST_n, vin2, out_reg_6);

  reg_7: general_register 
    generic map (9)
    port map (out_reg_6, CLK, RST_n, vin2, out_reg_7);

  --reg_10: general_register 
    --generic map (9)
    --port map (out_reg_6, CLK, RST_n, vin3, out_reg_10);

  reg_8: general_register 
    generic map (9)
    port map (std_logic_vector(out_mul_4), CLK, RST_n, vin2, out_reg_8);

  reg_9: general_register 
    generic map (9)
    port map (std_logic_vector(out_mul_5), CLK, RST_n, vin2, out_reg_9);
    
  reg_10: general_register 
    generic map (9)
    port map (std_logic_vector(out_mul_6), CLK, RST_n, vin2, out_reg_10);

  regout: general_register
    generic map (9)
    port map (std_logic_vector(out_add_5), CLK, RST_n, '1', DOUT_EXT);


-- the filter contains paths up to eight clock cycles long. The signal VIN must be delayed accordingly in order to
-- synchronize the internal registers. Two flip flops are added to this avail.

  reg_vin1: flipflopD
    port map (VIN, CLK, RST_n, '1', vin1); 
  
  reg_vin2: flipflopD
    port map (vin1, CLK, RST_n, '1', vin2);

  reg_vin3: flipflopD
    port map (vin2, CLK, RST_n, '1', vin3);

  reg_vin4: flipflopD
    port map (vin3, CLK, RST_n, '1', vin4);

  --reg_vin5: flipflopD
    --port map (vin4, CLK, RST_n, '1', vin5);




  out_mul_1_t <= signed(input) * (-signed(A1));
  out_mul_1 <= out_mul_1_t(24 downto 16);

  out_mul_2_t <= signed(out_reg_5) * signed(A1_A2);
  out_mul_2 <= out_mul_2_t(24 downto 16);

  out_mul_3_t <= signed(out_reg_4) * signed(A1_2_A2);
  out_mul_3 <= out_mul_3_t(24 downto 16);

  out_mul_4_t <= signed(out_reg_7) * signed(B2);
  out_mul_4 <= out_mul_4_t(24 downto 16);

  out_mul_5_t <= signed(out_reg_6) * signed(B1);
  out_mul_5 <= out_mul_5_t(24 downto 16);

  out_mul_6_t <= signed(out_reg_4) * signed(B0);
  out_mul_6 <= out_mul_6_t(24 downto 16);

  out_add_1 <= signed(out_reg_1) + signed(input);

  out_add_2 <= signed(out_reg_2) + signed(out_reg_3);

  out_add_3 <= out_add_2 + out_add_1;

  out_add_4 <= signed(out_reg_8) + signed(out_reg_9);

  out_add_5 <= out_add_4 + signed(out_reg_10);

  
  VOUT <= vin4 and RST_n;

end strct;
