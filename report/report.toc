\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Reference model development}{1}{chapter.1}%
\contentsline {section}{\numberline {1.1}Filter design and coefficient quantization using Matlab/Octave}{1}{section.1.1}%
\contentsline {section}{\numberline {1.2}Testing the filter and fixed point implementation}{2}{section.1.2}%
\contentsline {subsection}{\numberline {1.2.1}First step: Matlab/Octave pseudo-fixed-point}{2}{subsection.1.2.1}%
\contentsline {subsection}{\numberline {1.2.2}Second step: fixed-point C model}{2}{subsection.1.2.2}%
\contentsline {subsection}{\numberline {1.2.3}Comments on the results}{2}{subsection.1.2.3}%
\contentsline {chapter}{\numberline {2}VLSI Implementation}{4}{chapter.2}%
\contentsline {section}{\numberline {2.1}Architecture development}{4}{section.2.1}%
\contentsline {section}{\numberline {2.2}Simulation}{4}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}Testbench}{4}{subsection.2.2.1}%
\contentsline {subsection}{\numberline {2.2.2}Implementation Equivalence}{4}{subsection.2.2.2}%
\contentsline {section}{\numberline {2.3}Synthesis}{5}{section.2.3}%
\contentsline {section}{\numberline {2.4}Place \& Route}{6}{section.2.4}%
\contentsline {chapter}{\numberline {3}Advanced architecture development}{9}{chapter.3}%
\contentsline {section}{\numberline {3.1}J look ahead and throughput optimization}{9}{section.3.1}%
\contentsline {section}{\numberline {3.2}Synthesis}{10}{section.3.2}%
\contentsline {section}{\numberline {3.3}Place and Route}{10}{section.3.3}%
\contentsline {chapter}{\numberline {A}Simulation and Synthesis Scripts}{12}{appendix.A}%
\contentsline {section}{\numberline {A.1}First design Synthesis}{12}{section.A.1}%
\contentsline {section}{\numberline {A.2}Switching activity}{13}{section.A.2}%
\contentsline {subsection}{\numberline {A.2.1}Bash script}{13}{subsection.A.2.1}%
\contentsline {subsection}{\numberline {A.2.2}Simulation script}{13}{subsection.A.2.2}%
\contentsline {subsection}{\numberline {A.2.3}Synthesis script}{13}{subsection.A.2.3}%
\contentsline {section}{\numberline {A.3}Switching activity of the Placed and Routed netlist}{14}{section.A.3}%
\contentsline {subsection}{\numberline {A.3.1}Bash script}{14}{subsection.A.3.1}%
\contentsline {subsection}{\numberline {A.3.2}Simulation script}{14}{subsection.A.3.2}%
