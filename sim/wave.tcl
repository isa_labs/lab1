#add wave sim:/tb_iir_wv/CLK_i
#add wave sim:/tb_iir_wv/DIN_i
#add wave sim:/tb_iir_wv/VIN_i
#add wave sim:/tb_iir_wv/DOUT_i

#add wave sim:/tb_iir_wv/SM/vout
#add wave sim:/tb_iir_wv/SM/vin_down

add wave sim:/tb_iir_wv/CLK_i
add wave sim:/tb_iir_wv/UUT/vin
add wave sim:/tb_iir_wv/UUT/vin1
add wave sim:/tb_iir_wv/UUT/vin2
add wave sim:/tb_iir_wv/UUT/din
add wave sim:/tb_iir_wv/UUT/input
add wave sim:/tb_iir_wv/UUT/out_add_back0
add wave sim:/tb_iir_wv/UUT/out_add_back1
add wave sim:/tb_iir_wv/UUT/out_mul_back1
add wave sim:/tb_iir_wv/UUT/out_mul_back2
add wave sim:/tb_iir_wv/UUT/out_mul_fw0
add wave sim:/tb_iir_wv/UUT/out_mul_fw1
add wave sim:/tb_iir_wv/UUT/out_mul_fw2
add wave sim:/tb_iir_wv/UUT/out_add_fw0
add wave sim:/tb_iir_wv/UUT/out_add_fw1
add wave sim:/tb_iir_wv/UUT/out_reg0
add wave sim:/tb_iir_wv/UUT/out_reg1
add wave sim:/tb_iir_wv/UUT/vout
add wave sim:/tb_iir_wv/UUT/dout

run 3 us

