source /software/scripts/init_msim6.2g

rm -r work

vlib work

vcom -93 -work ./work ../tb/clk_gen.vhd
vcom -93 -work ./work ../tb/data_maker_new.vhd
vcom -93 -work ./work ../tb/data_sink.vhd
vlog -work ./work ../netlist/iir_filter.v
vlog -work ./work ../tb/tb_iir.v

vsim -c -L /software/dk/nangate45/verilog/msim6.2g -sdftyp /tb_iir/UUT=../netlist/iir_filter.sdf work.tb_iir -do ./sim_sw_act.tcl

cd ../syn/

source /software/scripts/init_synopsys_64.18

rm -r ./work/*

vcd2saif -input ../vcd/iir_filter_syn.vcd -output ../saif/iir_filter_syn.saif

dc_shell-xg-t -f ./sw_act.scr
