source /software/scripts/init_msim6.2g

rm -r work

vlib work

#compile
vcom -93 -work ./work ../tb/clk_gen.vhd
vcom -93 -work ./work ../tb/data_maker_new.vhd
vcom -93 -work ./work ../tb/data_sink_pr.vhd
vlog -work ./work ../innovus/iir_filter.v
vlog -work ./work ../tb/tb_iir_pr.v

#start simulation

vsim -c -L /software/dk/nangate45/verilog/msim6.2g -sdftyp /tb_iir_pr/UUT=../innovus/iir_filter.sdf work.tb_iir_pr -do ./sim_pr.tcl
