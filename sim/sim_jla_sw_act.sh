source /software/scripts/init_msim6.2g

rm -r work

vlib work

vcom -93 -work ./work ../tb/clk_gen.vhd
vcom -93 -work ./work ../tb/data_maker_jla_17bits.vhd
vcom -93 -work ./work ../tb/data_sink_jla.vhd
vlog -work ./work ../netlist/iir_filter_jla.v
vlog -work ./work ../tb/tb_iir_jla.v

vsim -c -L /software/dk/nangate45/verilog/msim6.2g -sdftyp /tb_iir_jla/UUT=../netlist/iir_filter_jla.sdf work.tb_iir_jla -do ./sim_jla_sw_act.tcl

cd ../syn/

source /software/scripts/init_synopsys_64.18

rm -r ./work/*

vcd2saif -input ../vcd/iir_filter_jla_syn.vcd -output ../saif/iir_filter_jla_syn.saif

dc_shell-xg-t -f ./sw_act_jla.scr
