source /software/scripts/init_msim6.2g

rm -r work

vlib work

#compile
vcom -93 -work ./work ../tb/clk_gen.vhd
vcom -93 -work ./work ../tb/data_maker_wv.vhd
vcom -93 -work ./work ../tb/data_sink_wv.vhd
vcom -93 -work ./work ../src/flipflopD.vhd
vcom -93 -work ./work ../src/general_register.vhd
vcom -93 -work ./work ../src/iir_filter.vhd
vlog -work ./work ../tb/tb_iir_wv.v

#start simulation
vsim -novopt work.tb_iir_wv -do ./wave.tcl


