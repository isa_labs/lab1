source /software/scripts/init_msim6.2g

rm -r work

vlib work

#compile
vcom -93 -work ./work ../tb/clk_gen.vhd
vcom -93 -work ./work ../tb/data_maker_jla_17bits.vhd
vcom -93 -work ./work ../tb/data_sink_pr_jla.vhd
vlog -work ./work ../innovus/iir_filter_jla.v
vlog -work ./work ../tb/tb_iir_pr_jla.v

#start simulation

vsim -c -L /software/dk/nangate45/verilog/msim6.2g -sdftyp /tb_iir_pr_jla/UUT=../innovus/iir_filter_jla.sdf work.tb_iir_pr_jla -do ./sim_pr_jla.tcl


