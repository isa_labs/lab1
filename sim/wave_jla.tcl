#add wave sim:/tb_iir_jla/CLK_i
#add wave sim:/tb_iir_jla/DIN_i
#add wave sim:/tb_iir_jla/VIN_i
#add wave sim:/tb_iir_jla/DOUT_i

#add wave sim:/tb_iir_jla/SM/vout
#add wave sim:/tb_iir_jla/SM/vin_down

add wave sim:/tb_iir_jla/CLK_i
add wave sim:/tb_iir_jla/UUT/vin
add wave sim:/tb_iir_jla/UUT/vin1
add wave sim:/tb_iir_jla/UUT/vin2
add wave sim:/tb_iir_jla/UUT/vin3
add wave sim:/tb_iir_jla/UUT/vin4
add wave sim:/tb_iir_jla/UUT/vin5
add wave sim:/tb_iir_jla/UUT/din
add wave sim:/tb_iir_jla/UUT/input
add wave sim:/tb_iir_jla/UUT/out_add_1
add wave sim:/tb_iir_jla/UUT/out_add_2
add wave sim:/tb_iir_jla/UUT/out_add_3
add wave sim:/tb_iir_jla/UUT/out_add_4
add wave sim:/tb_iir_jla/UUT/out_add_5
add wave sim:/tb_iir_jla/UUT/out_mul_1
add wave sim:/tb_iir_jla/UUT/out_mul_2
add wave sim:/tb_iir_jla/UUT/out_mul_3
add wave sim:/tb_iir_jla/UUT/out_mul_4
add wave sim:/tb_iir_jla/UUT/out_mul_5
add wave sim:/tb_iir_jla/UUT/out_mul_6
add wave sim:/tb_iir_jla/UUT/out_reg_1
add wave sim:/tb_iir_jla/UUT/out_reg_2
add wave sim:/tb_iir_jla/UUT/out_reg_3
add wave sim:/tb_iir_jla/UUT/out_reg_4
add wave sim:/tb_iir_jla/UUT/out_reg_5
add wave sim:/tb_iir_jla/UUT/out_reg_6
add wave sim:/tb_iir_jla/UUT/out_reg_7
add wave sim:/tb_iir_jla/UUT/out_reg_8
add wave sim:/tb_iir_jla/UUT/out_reg_9
add wave sim:/tb_iir_jla/UUT/out_reg_10
add wave sim:/tb_iir_jla/UUT/out_reg_11
add wave sim:/tb_iir_jla/UUT/out_reg_12
add wave sim:/tb_iir_jla/UUT/out_reg_13
add wave sim:/tb_iir_jla/UUT/vout
add wave sim:/tb_iir_jla/UUT/dout

run 3 us

