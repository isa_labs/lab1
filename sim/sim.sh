source /software/scripts/init_msim6.2g

rm -r work

vlib work

#compile
vcom -93 -work ./work ../tb/clk_gen.vhd
vcom -93 -work ./work ../tb/data_maker_new.vhd
vcom -93 -work ./work ../tb/data_sink.vhd
vcom -93 -work ./work ../src/flipflopD.vhd
vcom -93 -work ./work ../src/general_register.vhd
vcom -93 -work ./work ../src/iir_filter.vhd
vlog -work ./work ../tb/tb_iir.v

#start simulation
vsim -c -novopt work.tb_iir -do ./simulation.tcl


