source /software/scripts/init_msim6.2g

rm -r work

vlib work

#compile
vcom -93 -work ./work ../tb/clk_gen.vhd
vcom -93 -work ./work ../tb/data_maker_jla_wv.vhd
vcom -93 -work ./work ../tb/data_sink_jla_wv.vhd
vcom -93 -work ./work ../src/flipflopD.vhd
vcom -93 -work ./work ../src/general_register.vhd
vcom -93 -work ./work ../src/iir_filter_jla.vhd
vlog -work ./work ../tb/tb_iir_jla.v

#start simulation
#vsim -c -novopt work.tb_iir_jla -do ./simulation.tcl
vsim -novopt work.tb_iir_jla -do ./wave_jla.tcl


