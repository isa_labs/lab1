library verilog;
use verilog.vl_types.all;
entity iir_filter_jla is
    port(
        CLK             : in     vl_logic;
        RST_n           : in     vl_logic;
        DIN             : in     vl_logic_vector(8 downto 0);
        VIN             : in     vl_logic;
        A1              : in     vl_logic_vector(16 downto 0);
        A1_A2           : in     vl_logic_vector(16 downto 0);
        A1_2_A2         : in     vl_logic_vector(16 downto 0);
        B0              : in     vl_logic_vector(16 downto 0);
        B1              : in     vl_logic_vector(16 downto 0);
        B2              : in     vl_logic_vector(16 downto 0);
        DOUT            : out    vl_logic_vector(8 downto 0);
        VOUT            : out    vl_logic
    );
end iir_filter_jla;
